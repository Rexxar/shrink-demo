![roadmap_2017061421081326407.png](https://bitbucket.org/repo/p48rgrM/images/65080349-roadmap_2017061421081326407.png)

This project is demonstrating a tiny part of the **darkcity** library developed for [Disrupted Cities](http://frankiezafe.org/index.php?title=WorkUnit:Disrupted_Cities).

The problem solved (partially) here concerns 2D polygon *shrinking* (aka *offset*). The code has been mounted in an [openframeworks v9.0](http://openframeworks.cc/) project.

Please report issues in the [*issues* tab](https://bitbucket.org/polymorphteam/shrink-demo/issues).