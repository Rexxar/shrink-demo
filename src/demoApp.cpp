#include <vector>

#include "demoApp.h"

void demoApp::setup() {

    darkcity::Vec3<float>::UPz();
    cout << ">> UP<float>: " << darkcity::Vec3<float>::UP << endl;

    mouse_LD = false;
    mouse_RD = false;
    polygon_selected = false;
    shrink_factor = 0;

    shrink_in = true;
    shrink_out = true;
    shrink_debug = false;

    uisl_shrink.addListener( this, &demoApp::shrinkChanged );
    
    ui.setup();
    ui.add(uisl_shrink.setup("shrink", 0, 0, 300));
    ui.add(uitg_inward.setup("inward", shrink_in));
    ui.add(uitg_outward.setup("outward", shrink_out));
    ui.add(uitg_debug.setup("debug", shrink_debug));
    
    uitgs_selected = 0;
    uitgs_shapenum = 7;
    shapes_status = new bool[ uitgs_shapenum ];
    uitgs_shapes = new ofxToggle[uitgs_shapenum];
    for( uint8_t i = 0; i < uitgs_shapenum; ++i ) {
        shapes_status[i] = (i==0); 
        stringstream ss;
        ss << "shape [" << int(i) << "]";
        ui.add( uitgs_shapes[i].setup( ss.str(), shapes_status[i] ) );
        uitgs_shapes[i].addListener( this, &demoApp::toggleChanged );
    }
    
    multigon.allocate(50);
    testBatch(0);

    multigon_cp = multigon;
    if (multigon_cp.self_intersecting()) {
        multigon_cp.resolve_intersection();
    }
    
}

void demoApp::shrinkChanged( float& v ) {
    shrink_factor = v;
}

void demoApp::toggleChanged( bool& v ) {
    uint8_t uitgs_modified = uitgs_selected;
    for( uint8_t i = 0; i < uitgs_shapenum; ++i ) {
        if ( 
                uitgs_shapes[i].getParameter().cast<bool>() && 
                uitgs_shapes[i].getParameter().cast<bool>() != shapes_status[i]
            ) {
            uitgs_modified = i;
        }
    }
    if ( uitgs_modified != uitgs_selected ) {
        uitgs_selected = uitgs_modified;
        testBatch( uitgs_selected );
    }
    for ( uint8_t i = 0; i < uitgs_shapenum; ++i ) {
        if ( i != uitgs_selected ) {
           shapes_status[i] = false;
        } else {
           shapes_status[i] = true;
        }
        uitgs_shapes[i].getParameter().cast<bool>() = shapes_status[i];
    }
}

void demoApp::testBatch(int i) {

    switch (i) {

        default:
            break;

        case 0:
            for (int i = 0; i < multigon.point_num(); ++i) {
                float a = i * 1.f / multigon.point_num() * PI * 2;
                float m = 1;
                multigon[0][i].set(cos(a) * m, sin(a) * m, 0);
            }
            multigon.scale(150);
            multigon.translate(darkcity::Vec3<float>(300, 350, 0));
            multigon.process();
            previous_shrink_factor = -1000;
            break;
            
        case 1:
            multigon[0][0].set(332, 342, 0);
            multigon[0][1].set(448.817, 368.8, 0);
            multigon[0][2].set(445.287, 387.303, 0);
            multigon[0][3].set(439.466, 405.219, 0);
            multigon[0][4].set(431.446, 422.263, 0);
            multigon[0][5].set(421.353, 438.168, 0);
            multigon[0][6].set(409.345, 452.682, 0);
            multigon[0][7].set(395.614, 465.577, 0);
            multigon[0][8].set(380.374, 476.649, 0);
            multigon[0][9].set(363.867, 485.724, 0);
            multigon[0][10].set(346.353, 492.658, 0);
            multigon[0][11].set(255.107, 430.343, 0);
            multigon[0][12].set(309.419, 499.704, 0);
            multigon[0][13].set(290.581, 499.704, 0);
            multigon[0][14].set(271.893, 497.343, 0);
            multigon[0][15].set(253.647, 492.658, 0);
            multigon[0][16].set(236.133, 485.724, 0);
            multigon[0][17].set(219.626, 476.649, 0);
            multigon[0][18].set(204.386, 465.577, 0);
            multigon[0][19].set(190.655, 452.682, 0);
            multigon[0][20].set(178.647, 438.168, 0);
            multigon[0][21].set(168.554, 422.263, 0);
            multigon[0][22].set(160.534, 405.219, 0);
            multigon[0][23].set(154.713, 387.303, 0);
            multigon[0][24].set(151.183, 368.8, 0);
            multigon[0][25].set(150, 350, 0);
            multigon[0][26].set(151.183, 331.2, 0);
            multigon[0][27].set(154.713, 312.697, 0);
            multigon[0][28].set(160.534, 294.781, 0);
            multigon[0][29].set(168.554, 277.737, 0);
            multigon[0][30].set(178.647, 261.832, 0);
            multigon[0][31].set(249.655, 397.318, 0);
            multigon[0][32].set(204.386, 234.423, 0);
            multigon[0][33].set(219.626, 223.351, 0);
            multigon[0][34].set(236.133, 214.276, 0);
            multigon[0][35].set(293.647, 296.342, 0);
            multigon[0][36].set(271.893, 202.657, 0);
            multigon[0][37].set(290.581, 200.296, 0);
            multigon[0][38].set(309.419, 200.296, 0);
            multigon[0][39].set(328.107, 202.657, 0);
            multigon[0][40].set(346.353, 207.342, 0);
            multigon[0][41].set(255.867, 411.276, 0);
            multigon[0][42].set(380.374, 223.351, 0);
            multigon[0][43].set(395.614, 234.423, 0);
            multigon[0][44].set(409.345, 247.318, 0);
            multigon[0][45].set(421.353, 261.832, 0);
            multigon[0][46].set(431.446, 277.737, 0);
            multigon[0][47].set(439.466, 294.781, 0);
            multigon[0][48].set(445.287, 312.697, 0);
            multigon[0][49].set(448.817, 331.2, 0);
            multigon.process();
            previous_shrink_factor = -1000;
            break;

        case 2:
            multigon[0][0].set(313.977, 368, 0);
            multigon[0][1].set(465.57, 366.674, 0);
            multigon[0][2].set(488.685, 386.135, 0);
            multigon[0][3].set(417.419, 448.388, 0);
            multigon[0][4].set(403.226, 412.237, 0);
            multigon[0][5].set(386.518, 395.015, 0);
            multigon[0][6].set(357.793, 388.31, 0);
            multigon[0][7].set(330.577, 397.874, 0);
            multigon[0][8].set(319.838, 419.743, 0);
            multigon[0][9].set(321.911, 447.943, 0);
            multigon[0][10].set(340, 472.496, 0);
            multigon[0][11].set(376.477, 485.039, 0);
            multigon[0][12].set(254.698, 599.038, 0);
            multigon[0][13].set(202.814, 558.899, 0);
            multigon[0][14].set(226.028, 553.359, 0);
            multigon[0][15].set(244.341, 531.379, 0);
            multigon[0][16].set(248.217, 494.547, 0);
            multigon[0][17].set(230.343, 466.823, 0);
            multigon[0][18].set(197.313, 461.919, 0);
            multigon[0][19].set(170.63, 471.4, 0);
            multigon[0][20].set(161.631, 497.273, 0);
            multigon[0][21].set(166.597, 528.182, 0);
            multigon[0][22].set(95.279, 460.842, 0);
            multigon[0][23].set(126.375, 368.106, 0);
            multigon[0][24].set(369.43, 323.39, 0);
            multigon[0][25].set(117.722, 350, 0);
            multigon[0][26].set(114.787, 329.381, 0);
            multigon[0][27].set(113.143, 302.442, 0);
            multigon[0][28].set(115.478, 280.694, 0);
            multigon[0][29].set(119.286, 255.49, 0);
            multigon[0][30].set(145.909, 204.226, 0);
            multigon[0][31].set(163.279, 241.721, 0);
            multigon[0][32].set(153.338, 177.98, 0);
            multigon[0][33].set(186.994, 180.992, 0);
            multigon[0][34].set(233.705, 209.115, 0);
            multigon[0][35].set(247.351, 187.963, 0);
            multigon[0][36].set(283.134, 307.922, 0);
            multigon[0][37].set(249.594, 40.7071, 0);
            multigon[0][38].set(297.916, 154.392, 0);
            multigon[0][39].set(314.296, 135.91, 0);
            multigon[0][40].set(336.303, 166.572, 0);
            multigon[0][41].set(357.569, 185.283, 0);
            multigon[0][42].set(317.271, 305.513, 0);
            multigon[0][43].set(435.216, 233.563, 0);
            multigon[0][44].set(432.77, 254.26, 0);
            multigon[0][45].set(435.632, 271.551, 0);
            multigon[0][46].set(444.262, 284.943, 0);
            multigon[0][47].set(454.489, 297.417, 0);
            multigon[0][48].set(469.372, 304.688, 0);
            multigon[0][49].set(462.881, 355.518, 0);
            multigon.process();
            previous_shrink_factor = -1000;
            break;

        case 3:
            multigon[0][0].set(434.977, 358, 0);
            multigon[0][1].set(465.57, 366.674, 0);
            multigon[0][2].set(488.685, 386.135, 0);
            multigon[0][3].set(417.419, 448.388, 0);
            multigon[0][4].set(403.226, 412.237, 0);
            multigon[0][5].set(386.518, 395.015, 0);
            multigon[0][6].set(357.793, 388.31, 0);
            multigon[0][7].set(330.577, 397.874, 0);
            multigon[0][8].set(319.838, 419.743, 0);
            multigon[0][9].set(321.911, 447.943, 0);
            multigon[0][10].set(340, 472.496, 0);
            multigon[0][11].set(376.477, 485.039, 0);
            multigon[0][12].set(254.698, 599.038, 0);
            multigon[0][13].set(202.814, 558.899, 0);
            multigon[0][14].set(226.028, 553.359, 0);
            multigon[0][15].set(244.341, 531.379, 0);
            multigon[0][16].set(248.217, 494.547, 0);
            multigon[0][17].set(230.343, 466.823, 0);
            multigon[0][18].set(197.313, 461.919, 0);
            multigon[0][19].set(170.63, 471.4, 0);
            multigon[0][20].set(161.631, 497.273, 0);
            multigon[0][21].set(166.597, 528.182, 0);
            multigon[0][22].set(95.279, 460.842, 0);
            multigon[0][23].set(126.375, 368.106, 0);
            multigon[0][24].set(213.43, 307.39, 0);
            multigon[0][25].set(117.722, 350, 0);
            multigon[0][26].set(114.787, 329.381, 0);
            multigon[0][27].set(113.143, 302.442, 0);
            multigon[0][28].set(115.478, 280.694, 0);
            multigon[0][29].set(119.286, 255.49, 0);
            multigon[0][30].set(145.909, 204.226, 0);
            multigon[0][31].set(163.279, 241.721, 0);
            multigon[0][32].set(153.338, 177.98, 0);
            multigon[0][33].set(186.994, 180.992, 0);
            multigon[0][34].set(233.705, 209.115, 0);
            multigon[0][35].set(247.351, 187.963, 0);
            multigon[0][36].set(268.134, 241.922, 0);
            multigon[0][37].set(249.594, 40.7071, 0);
            multigon[0][38].set(297.916, 154.392, 0);
            multigon[0][39].set(314.296, 135.91, 0);
            multigon[0][40].set(336.303, 166.572, 0);
            multigon[0][41].set(357.569, 185.283, 0);
            multigon[0][42].set(364.271, 232.513, 0);
            multigon[0][43].set(435.216, 233.563, 0);
            multigon[0][44].set(432.77, 254.26, 0);
            multigon[0][45].set(435.632, 271.551, 0);
            multigon[0][46].set(444.262, 284.943, 0);
            multigon[0][47].set(454.489, 297.417, 0);
            multigon[0][48].set(469.372, 304.688, 0);
            multigon[0][49].set(462.881, 355.518, 0);
            multigon.process();
            previous_shrink_factor = -1000;
            break;

        case 4:
            multigon[0][0].set(450, 350, 0);
            multigon[0][1].set(448.817, 368.8, 0);
            multigon[0][2].set(445.287, 387.303, 0);
            multigon[0][3].set(439.466, 405.219, 0);
            multigon[0][4].set(431.446, 422.263, 0);
            multigon[0][5].set(421.353, 438.168, 0);
            multigon[0][6].set(409.345, 452.682, 0);
            multigon[0][7].set(395.614, 465.577, 0);
            multigon[0][8].set(380.374, 476.649, 0);
            multigon[0][9].set(363.867, 485.724, 0);
            multigon[0][10].set(346.353, 492.658, 0);
            multigon[0][11].set(328.107, 497.343, 0);
            multigon[0][12].set(309.419, 499.704, 0);
            multigon[0][13].set(290.581, 499.704, 0);
            multigon[0][14].set(271.893, 497.343, 0);
            multigon[0][15].set(253.647, 492.658, 0);
            multigon[0][16].set(236.133, 485.724, 0);
            multigon[0][17].set(219.626, 476.649, 0);
            multigon[0][18].set(204.386, 465.577, 0);
            multigon[0][19].set(190.655, 452.682, 0);
            multigon[0][20].set(178.647, 438.168, 0);
            multigon[0][21].set(168.554, 422.263, 0);
            multigon[0][22].set(160.534, 405.219, 0);
            multigon[0][23].set(154.713, 387.303, 0);
            multigon[0][24].set(151.183, 368.8, 0);
            multigon[0][25].set(150, 350, 0);
            multigon[0][26].set(151.183, 331.2, 0);
            multigon[0][27].set(154.713, 312.697, 0);
            multigon[0][28].set(160.534, 294.781, 0);
            multigon[0][29].set(168.554, 277.737, 0);
            multigon[0][30].set(178.647, 261.832, 0);
            multigon[0][31].set(190.655, 247.318, 0);
            multigon[0][32].set(204.386, 234.423, 0);
            multigon[0][33].set(219.626, 223.351, 0);
            multigon[0][34].set(236.133, 214.276, 0);
            multigon[0][35].set(253.647, 207.342, 0);
            multigon[0][36].set(271.893, 202.657, 0);
            multigon[0][37].set(290.581, 200.296, 0);
            multigon[0][38].set(309.419, 200.296, 0);
            multigon[0][39].set(328.107, 202.657, 0);
            multigon[0][40].set(265.353, 412.342, 0);
            multigon[0][41].set(363.867, 214.276, 0);
            multigon[0][42].set(380.374, 223.351, 0);
            multigon[0][43].set(395.614, 234.423, 0);
            multigon[0][44].set(409.345, 247.318, 0);
            multigon[0][45].set(421.353, 261.832, 0);
            multigon[0][46].set(431.446, 277.737, 0);
            multigon[0][47].set(439.466, 294.781, 0);
            multigon[0][48].set(445.287, 312.697, 0);
            multigon[0][49].set(448.817, 331.2, 0);
            multigon.process();
            previous_shrink_factor = -1000;
            break;

        case 5:
            multigon[0][0].set(450, 350, 0);
            multigon[0][1].set(448.817, 368.8, 0);
            multigon[0][2].set(445.287, 387.303, 0);
            multigon[0][3].set(439.466, 405.219, 0);
            multigon[0][4].set(431.446, 422.263, 0);
            multigon[0][5].set(421.353, 438.168, 0);
            multigon[0][6].set(409.345, 452.682, 0);
            multigon[0][7].set(395.614, 465.577, 0);
            multigon[0][8].set(380.374, 476.649, 0);
            multigon[0][9].set(363.867, 485.724, 0);
            multigon[0][10].set(346.353, 492.658, 0);
            multigon[0][11].set(328.107, 497.343, 0);
            multigon[0][12].set(309.419, 499.704, 0);
            multigon[0][13].set(290.581, 499.704, 0);
            multigon[0][14].set(271.893, 497.343, 0);
            multigon[0][15].set(253.647, 492.658, 0);
            multigon[0][16].set(236.133, 485.724, 0);
            multigon[0][17].set(219.626, 476.649, 0);
            multigon[0][18].set(204.386, 465.577, 0);
            multigon[0][19].set(190.655, 452.682, 0);
            multigon[0][20].set(178.647, 438.168, 0);
            multigon[0][21].set(168.554, 422.263, 0);
            multigon[0][22].set(160.534, 405.219, 0);
            multigon[0][23].set(154.713, 387.303, 0);
            multigon[0][24].set(151.183, 368.8, 0);
            multigon[0][25].set(150, 350, 0);
            multigon[0][26].set(151.183, 331.2, 0);
            multigon[0][27].set(154.713, 312.697, 0);
            multigon[0][28].set(160.534, 294.781, 0);
            multigon[0][29].set(168.554, 277.737, 0);
            multigon[0][30].set(178.647, 261.832, 0);
            multigon[0][31].set(190.655, 247.318, 0);
            multigon[0][32].set(204.386, 234.423, 0);
            multigon[0][33].set(219.626, 223.351, 0);
            multigon[0][34].set(236.133, 214.276, 0);
            multigon[0][35].set(253.647, 207.342, 0);
            multigon[0][36].set(271.893, 202.657, 0);
            multigon[0][37].set(290.581, 200.296, 0);
            multigon[0][38].set(309.419, 200.296, 0);
            multigon[0][39].set(328.107, 202.657, 0);
            multigon[0][40].set(425.353, 317.342, 0);
            multigon[0][41].set(363.867, 214.276, 0);
            multigon[0][42].set(380.374, 223.351, 0);
            multigon[0][43].set(395.614, 234.423, 0);
            multigon[0][44].set(409.345, 247.318, 0);
            multigon[0][45].set(421.353, 261.832, 0);
            multigon[0][46].set(431.446, 277.737, 0);
            multigon[0][47].set(439.466, 294.781, 0);
            multigon[0][48].set(445.287, 312.697, 0);
            multigon[0][49].set(448.817, 331.2, 0);
            multigon.process();
            previous_shrink_factor = -1000;
            break;

        case 6:
            multigon[0][0].set(450, 350, 0);
            multigon[0][1].set(448.817, 368.8, 0);
            multigon[0][2].set(445.287, 387.303, 0);
            multigon[0][3].set(439.466, 405.219, 0);
            multigon[0][4].set(431.446, 422.263, 0);
            multigon[0][5].set(421.353, 438.168, 0);
            multigon[0][6].set(409.345, 452.682, 0);
            multigon[0][7].set(395.614, 465.577, 0);
            multigon[0][8].set(380.374, 476.649, 0);
            multigon[0][9].set(363.867, 485.724, 0);
            multigon[0][10].set(346.353, 492.658, 0);
            multigon[0][11].set(328.107, 497.343, 0);
            multigon[0][12].set(309.419, 499.704, 0);
            multigon[0][13].set(290.581, 499.704, 0);
            multigon[0][14].set(271.893, 497.343, 0);
            multigon[0][15].set(253.647, 492.658, 0);
            multigon[0][16].set(236.133, 485.724, 0);
            multigon[0][17].set(219.626, 476.649, 0);
            multigon[0][18].set(204.386, 465.577, 0);
            multigon[0][19].set(190.655, 452.682, 0);
            multigon[0][20].set(178.647, 438.168, 0);
            multigon[0][21].set(168.554, 422.263, 0);
            multigon[0][22].set(160.534, 405.219, 0);
            multigon[0][23].set(154.713, 387.303, 0);
            multigon[0][24].set(151.183, 368.8, 0);
            multigon[0][25].set(150, 350, 0);
            multigon[0][26].set(151.183, 331.2, 0);
            multigon[0][27].set(154.713, 312.697, 0);
            multigon[0][28].set(160.534, 294.781, 0);
            multigon[0][29].set(168.554, 277.737, 0);
            multigon[0][30].set(178.647, 261.832, 0);
            multigon[0][31].set(190.655, 247.318, 0);
            multigon[0][32].set(204.386, 234.423, 0);
            multigon[0][33].set(219.626, 223.351, 0);
            multigon[0][34].set(236.133, 214.276, 0);
            multigon[0][35].set(253.647, 207.342, 0);
            multigon[0][36].set(271.893, 202.657, 0);
            multigon[0][37].set(290.581, 200.296, 0);
            multigon[0][38].set(309.419, 130.296, 0);
            multigon[0][39].set(328.107, 202.657, 0);
            multigon[0][40].set(425.353, 317.342, 0);
            multigon[0][41].set(363.867, 169.276, 0);
            multigon[0][42].set(398.374, 193.351, 0);
            multigon[0][43].set(423.614, 220.423, 0);
            multigon[0][44].set(409.345, 247.318, 0);
            multigon[0][45].set(421.353, 261.832, 0);
            multigon[0][46].set(431.446, 277.737, 0);
            multigon[0][47].set(439.466, 294.781, 0);
            multigon[0][48].set(445.287, 312.697, 0);
            multigon[0][49].set(448.817, 331.2, 0);
            multigon.process();
            previous_shrink_factor = -1000;
            break;

        case 7:
            multigon[0][0].set(456.977, 365, 0);
            multigon[0][1].set(455.57, 385.674, 0);
            multigon[0][2].set(447.685, 409.135, 0);
            multigon[0][3].set(417.419, 448.388, 0);
            multigon[0][4].set(403.226, 412.237, 0);
            multigon[0][5].set(386.518, 395.015, 0);
            multigon[0][6].set(357.793, 388.31, 0);
            multigon[0][7].set(343.577, 356.874, 0);
            multigon[0][8].set(319.838, 419.743, 0);
            multigon[0][9].set(321.911, 447.943, 0);
            multigon[0][10].set(340, 472.496, 0);
            multigon[0][11].set(376.477, 485.039, 0);
            multigon[0][12].set(254.698, 599.038, 0);
            multigon[0][13].set(202.814, 558.899, 0);
            multigon[0][14].set(226.028, 553.359, 0);
            multigon[0][15].set(244.341, 531.379, 0);
            multigon[0][16].set(248.217, 494.547, 0);
            multigon[0][17].set(230.343, 466.823, 0);
            multigon[0][18].set(197.313, 461.919, 0);
            multigon[0][19].set(170.63, 471.4, 0);
            multigon[0][20].set(161.631, 497.273, 0);
            multigon[0][21].set(166.597, 528.182, 0);
            multigon[0][22].set(96.279, 450.842, 0);
            multigon[0][23].set(89.375, 407.106, 0);
            multigon[0][24].set(88.43, 370.39, 0);
            multigon[0][25].set(88.722, 347, 0);
            multigon[0][26].set(91.787, 320.381, 0);
            multigon[0][27].set(96.143, 294.442, 0);
            multigon[0][28].set(100.478, 270.694, 0);
            multigon[0][29].set(109.286, 244.49, 0);
            multigon[0][30].set(118.909, 221.226, 0);
            multigon[0][31].set(133.279, 199.721, 0);
            multigon[0][32].set(153.338, 177.98, 0);
            multigon[0][33].set(173.994, 164.992, 0);
            multigon[0][34].set(194.705, 158.115, 0);
            multigon[0][35].set(216.351, 152.963, 0);
            multigon[0][36].set(240.134, 147.922, 0);
            multigon[0][37].set(261.594, 144.707, 0);
            multigon[0][38].set(285.916, 144.392, 0);
            multigon[0][39].set(311.296, 149.91, 0);
            multigon[0][40].set(336.303, 166.572, 0);
            multigon[0][41].set(356.569, 182.283, 0);
            multigon[0][42].set(378.271, 201.513, 0);
            multigon[0][43].set(396.216, 219.563, 0);
            multigon[0][44].set(414.77, 242.26, 0);
            multigon[0][45].set(427.632, 260.551, 0);
            multigon[0][46].set(438.262, 278.943, 0);
            multigon[0][47].set(448.489, 295.417, 0);
            multigon[0][48].set(455.372, 319.688, 0);
            multigon[0][49].set(457.881, 342.518, 0);
            multigon.process();
            previous_shrink_factor = -1000;
            break;

    }

}

//--------------------------------------------------------------

void demoApp::update() {

    if ( uitg_inward.getParameter().cast<bool>() != shrink_in ) {
        shrink_in = uitg_inward.getParameter().cast<bool>();
        previous_shrink_factor = -1000;
    }
    
    if ( uitg_outward.getParameter().cast<bool>() != shrink_out ) {
        shrink_out = uitg_outward.getParameter().cast<bool>();
        previous_shrink_factor = -1000;
    }
    
    if ( uitg_debug.getParameter().cast<bool>() != shrink_debug ) {
        shrink_debug = uitg_debug.getParameter().cast<bool>();
        previous_shrink_factor = -1000;
    }

}

void demoApp::updateShrink() {

    bool bypass = false;

    if (bypass || previous_shrink_factor != shrink_factor) {

#ifdef DARKCITY_MULTIGON_DEBUG
        cout << endl << "***********************" << endl;
#endif

        multigon_cp = multigon;
        if (multigon_cp.self_intersecting()) {
            multigon_cp.resolve_intersection();
        }

        std::vector< darkcity::Multigon<float>* >::iterator itmg;
        std::vector< darkcity::Multigon<float>* >::iterator itmge;
        itmg = mgons_in.begin();
        itmge = mgons_in.end();
        for (; itmg != itmge; ++itmg) {
            delete (*itmg);
        }
        mgons_in.clear();
        itmg = mgons_out.begin();
        itmge = mgons_out.end();
        for (; itmg != itmge; ++itmg) {
            delete (*itmg);
        }
        mgons_out.clear();

        uint8_t count;
        float sfactor;
        darkcity::Multigon<float>* previous;

        // IN
        if ( shrink_in ) {
            sfactor = shrink_factor;
            previous = &multigon_cp;
            count = 0;
            while (sfactor > 0) {

    #ifdef DARKCITY_MULTIGON_DEBUG
                cout << "** shrink, iteration " << int( count) << ", factor: " << sfactor << endl;
    #endif
                float consumed = 0;
                darkcity::Multigon<float>* mg = new darkcity::Multigon<float>();
                consumed = (*previous).shrink(sfactor, (*mg));
                previous = mg;
                sfactor -= consumed;
                ++count;
                if (consumed <= 0) {
                    delete mg;
    #ifdef DARKCITY_MULTIGON_DEBUG
                    cout << "ERROR! " << consumed << endl;
    #endif
                    break;
                }
                if (mg->shape_num() <= 0) {
                    delete mg;
                    //                cout << "END OF SITE" << endl;
                    break;
                }
                mgons_in.push_back(mg);
            }
        }

        //OUT
        if ( shrink_out ) {
            sfactor = -shrink_factor;
            previous = &multigon_cp;
            count = 0;
            while (sfactor < 0) {
    #ifdef DARKCITY_MULTIGON_DEBUG
                cout << "** inflate, iteration " << int( count) << ", factor: " << sfactor << endl;
    #endif
                float consumed = 0;
                darkcity::Multigon<float>* mg = new darkcity::Multigon<float>();
                consumed = (*previous).shrink(sfactor, (*mg));
                previous = mg;
                sfactor -= consumed;
                ++count;
                if (consumed >= 0) {
                    delete mg;
                    //                cout << "ERROR! " << consumed << endl;
                    break;
                }
                if (mg->shape_num() <= 0) {
                    delete mg;
                    //                cout << "END OF SITE" << endl;
                    break;
                }
                mgons_out.push_back(mg);
            }
        }

        previous_shrink_factor = shrink_factor;

    }

}

//--------------------------------------------------------------

void demoApp::draw(darkcity::Multigon<float>& dcp, float opacity, bool debug) {

    std::stringstream ss;

    for (uint8_t s = 0; s < dcp.shape_num(); ++s) {

        darkcity::Point<float>* first = dcp.first(s);
        darkcity::Point<float>* p = first;

        if (dcp.shape_num() == 1) {

            glColor4f(1, 1, 1, opacity);

        } else {

            ofSetColor(
                    ofColor::fromHsb(
                    s * 1.f / dcp.shape_num() * 255,
                    255, 255, 255 * opacity
                    )
                    );

        }

        //        if (dcp[s].self_intersection) glColor4f(1, 0, 0, opacity);
        glBegin(GL_LINES);
        while (p) {
            glVertex3f(p->previous->x, p->previous->y, p->previous->z);
            glVertex3f(p->x, p->y, p->z);
            p = p->next;
            if (p == first) {
                break;
            }
        }
        glEnd();

        p = first;
        glColor4f(1, 1, 1, opacity);
        glPointSize(5 * opacity);
        glBegin(GL_POINTS);
        while (p) {
            glVertex3f(p->x, p->y, p->z);
            p = p->next;
            if (p == first) {
                break;
            }
        }
        glEnd();

        p = first;
        ofSetColor(255, 255, 255);
        int id = 0;
        while (p) {
            ss << id << ": " << p->x << ", " << p->y << ", " << p->z <<
                    std::endl;
            ++id;
            p = p->next;
            if (p == first) {
                break;
            }
        }

        if (dcp[s].point_num == 0) {
            glColor4f(0, 1, 0, 0.5);
            ofDrawLine(
                    dcp[s].center.x - 5, dcp[s].center.y, dcp[s].center.z,
                    dcp[s].center.x + 5, dcp[s].center.y, dcp[s].center.z
                    );
            ofDrawLine(
                    dcp[s].center.x, dcp[s].center.y - 5, dcp[s].center.z,
                    dcp[s].center.x, dcp[s].center.y + 5, dcp[s].center.z
                    );
            stringstream ssc;
            ssc << int(s) << " eos";
            glColor4f(1, 1, 1, 1);
            ofDrawBitmapString(
                    ssc.str(),
                    dcp[s].center.x + 2, dcp[s].center.y + 12
                    );
        }

        if (debug) {

            glColor3f(0, 1, 1);
            glBegin(GL_LINES);
            p = first;
            while (p) {
                if (!p->concav) {
                    glVertex3f(p->x, p->y, p->z);
                    glVertex3f(
                            p->x + p->normal.x * 20,
                            p->y + p->normal.y * 20,
                            p->z + p->normal.z * 20
                            );
                }
                p = p->next;
                if (p == first) {
                    break;
                }

            }
            glEnd();

            glColor3f(1, 0, 1);
            glBegin(GL_LINES);
            p = first;
            while (p) {
                if (p->concav) {
                    glVertex3f(p->x, p->y, p->z);
                    glVertex3f(
                            p->x + p->normal.x * 20,
                            p->y + p->normal.y * 20,
                            p->z + p->normal.z * 20
                            );
                }
                p = p->next;
                if (p == first) {
                    break;
                }

            }
            glEnd();

            if (dcp[s].edgeevent_in.pt) {
                darkcity::EdgeEvent<float>& ev = dcp[s].edgeevent_in;
                glColor4f(1, 0, 0, opacity);
                ofDrawCircle(ev.pt->x, ev.pt->y, ev.pt->z, 2);
                ofDrawLine(
                        ev.position.x, ev.position.y, ev.position.z,
                        ev.pt->x, ev.pt->y, ev.pt->z);
            }


            // debugging edege events
            //            for (uint16_t i = 0; i < dcp[s].point_num; ++i) {
            //                if (!dcp[s].edgeevents_in[i]) continue;
            //                darkcity::EdgeEvent<float>& ev = (*dcp[s].edgeevents_in[i]);
            //                if (ev.pt == dcp[s].edgeevent_in.pt) {
            //                    glColor4f(1, 0, 0, opacity);
            //                } else {
            //                    glColor4f(1, 1, 1, opacity * 0.5);
            //                }
            //                ofDrawCircle(ev.position.x, ev.position.y, ev.position.z, 2);
            //                ofSetLineWidth(3);
            //                darkcity::Vec3<float> shrink =
            //                        (*ev.pt) + (ev.position - (*ev.pt)).normalized() * ev.shrink;
            //                ofDrawLine(
            //                        shrink.x, shrink.y, shrink.z,
            //                        ev.pt->x, ev.pt->y, ev.pt->z);
            //                ofSetLineWidth(1);
            //                ofDrawLine(
            //                        ev.position.x, ev.position.y, ev.position.z,
            //                        ev.pt->x, ev.pt->y, ev.pt->z);
            //            }
            //
            //            for (uint16_t i = 0; i < dcp[s].point_num; ++i) {
            //                if (!dcp[s].edgeevents_out[i]) continue;
            //                darkcity::EdgeEvent<float>& ev = (*dcp[s].edgeevents_out[i]);
            //                if (ev.pt == dcp[s].edgeevent_out.pt) {
            //                    glColor4f(0, 1, 0, opacity);
            //                } else {
            //                    glColor4f(1, 1, 1, opacity * 0.5);
            //                }
            //                ofDrawCircle(ev.position.x, ev.position.y, ev.position.z, 2);
            //                ofSetLineWidth(3);
            //                darkcity::Vec3<float> shrink =
            //                        (*ev.pt) + (ev.position - (*ev.pt)).normalized() * ev.shrink;
            //                ofDrawLine(
            //                        shrink.x, shrink.y, shrink.z,
            //                        ev.pt->x, ev.pt->y, ev.pt->z);
            //                ofSetLineWidth(1);
            //                ofDrawLine(
            //                        ev.position.x, ev.position.y, ev.position.z,
            //                        ev.pt->x, ev.pt->y, ev.pt->z);
            //            }

            // debugging split events
            for (uint16_t i = 0; i < dcp[s].point_num; ++i) {

                if (
                        dcp[s].splitevents_in[i]
                        &&
                        &(dcp[s][i]) == dcp[s].splitevent_in_01->pt
                        ) {

                    darkcity::SplitEvent<float>& ev = (*dcp[s].splitevents_in[i]);

//                    if ( &(dcp[s][i]) == dcp[s].splitevent_in.pt ) {

                    glColor4f(1, 0, 1, opacity);
                    ofDrawCircle(ev.event_position.x, ev.event_position.y, ev.event_position.z, 2);
                    ofDrawLine(
                            ev.event_position.x, ev.event_position.y, ev.event_position.z,
                            ev.pt->x, ev.pt->y, ev.pt->z);
                    darkcity::Vec3<float> segmid = (*ev.segA) + ((*ev.segB)-(*ev.segA)) * 0.5;
                    glColor3f(0, 1, 0);
                    darkcity::Vec3<float> target = ev.seg_projection;
                    ofDrawLine(
                            ev.event_position.x, ev.event_position.y, ev.event_position.z,
                            target.x, target.y, target.z);
                    ofDrawLine(
                            segmid.x, segmid.y, segmid.z,
                            target.x, target.y, target.z);

                    // normals
                    glColor4f(1, 1, 1, 0.5);
                    ofDrawLine(
                            ev.pt->x, ev.pt->y, ev.pt->z,
                            ev.pt->x - ev.pt->normal.x * ev.distance,
                            ev.pt->y - ev.pt->normal.y * ev.distance,
                            ev.pt->z - ev.pt->normal.z * ev.distance
                            );
                    //                    ofDrawLine(
                    //                            ev.segA->x, ev.segA->y, ev.segA->z,
                    //                            ev.segA->x - ev.segA->normal.x * 200,
                    //                            ev.segA->y - ev.segA->normal.y * 200,
                    //                            ev.segA->z - ev.segA->normal.z * 200
                    //                            );
                    //                    ofDrawLine(
                    //                            ev.segB->x, ev.segB->y, ev.segB->z,
                    //                            ev.segB->x - ev.segB->normal.x * 200,
                    //                            ev.segB->y - ev.segB->normal.y * 200,
                    //                            ev.segB->z - ev.segB->normal.z * 200
                    //                            );

                    //}

                    glColor3f(1, 1, 1);
                    stringstream sse;
                    sse << i << "-[" <<
                            ev.segA_index << "," <<
                            ev.segB_index << "]: " <<
                            ev.shrink;
                    ofDrawBitmapString(
                            sse.str(),
                            ev.pt->x, ev.pt->y
                            );

                }

                if (
                        dcp[s].splitevents_out[i]
                        &&
                        &(dcp[s][i]) == dcp[s].splitevent_out_01->pt
                        ) {
                    
                    darkcity::SplitEvent<float>& ev = (*dcp[s].splitevents_out[i]);
                    glColor4f(1, 1, 0, opacity);
                    ofDrawCircle(ev.event_position.x, ev.event_position.y, ev.event_position.z, 2);
                    ofDrawLine(
                            ev.event_position.x, ev.event_position.y, ev.event_position.z,
                            ev.pt->x, ev.pt->y, ev.pt->z);
                    darkcity::Vec3<float> segmid = (*ev.segA) + ((*ev.segB)-(*ev.segA)) * 0.5;
                    glColor3f(0, 0, 1);
                    darkcity::Vec3<float> target = ev.seg_projection;
                    ofDrawLine(
                            ev.event_position.x, ev.event_position.y, ev.event_position.z,
                            target.x, target.y, target.z);
                    ofDrawLine(
                            segmid.x, segmid.y, segmid.z,
                            target.x, target.y, target.z);

                    glColor3f(1, 1, 1);
                    stringstream sse;
                    sse << i << ":" << ev.shrink;
                    ofDrawBitmapString(
                            sse.str(),
                            ev.event_position.x, ev.event_position.y
                            );

                }

            }

            //            ofFill();
            //            glColor3f(1, 1, 1);
            //            p = first;
            //            while (p) {
            //
            //                if (p->sharp && !p->concav) {
            //                    //                    float a = ( p->dir_angle - p->previous->dir_angle );
            //                    //                    stringstream ss;
            //                    //                    ss << int( a * 180 / M_PI);
            //                    //                    ofDrawBitmapString(ss.str(), p->x, p->y);
            //                    ofDrawCircle(p->x, p->y, 5);
            //                }
            //                // angles info
            //                p = p->next;
            //                if (p == first) {
            //                    break;
            //                }
            //
            //            }
            //            ofNoFill();


        }

    }

}

void demoApp::draw() {

    ofBackground(80, 80, 80);

    uint64_t starttime = ofGetElapsedTimeMicros();
    
    draw(multigon, 1);

    ofPushMatrix();

    ofTranslate(450, 0, 0);

    draw(multigon_cp, 0.5);

    updateShrink();

    std::vector< darkcity::Multigon<float>* >::iterator itmg;
    std::vector< darkcity::Multigon<float>* >::iterator itmge;
    std::vector< darkcity::Multigon<float>* >::iterator itmglast;

    itmg = mgons_in.begin();
    itmge = mgons_in.end();
    itmglast = mgons_in.end();
    --itmglast;
    for (; itmg != itmge; ++itmg) {
        float opacity = 0.2;
        bool debug = false;
        if (itmg == itmglast) {
            opacity = 1;
            if ( shrink_debug ) debug = true;
        }
        if ( shrink_debug || itmg == itmglast ) {
            draw((*(*itmg)), opacity, debug);
        }
    }

    itmg = mgons_out.begin();
    itmge = mgons_out.end();
    itmglast = mgons_out.end();
    --itmglast;
    for (; itmg != itmge; ++itmg) {
        float opacity = 0.2;
        bool debug = false;
        if (itmg == itmglast) {
            opacity = 1;
            if ( shrink_debug ) debug = true;
        }
        if ( shrink_debug || itmg == itmglast ) {
            draw((*(*itmg)), opacity, debug);
        }
    }


    ofPopMatrix();

    //    ofSetColor(255, 255, 255);
    //    ofDrawBitmapString(ss.str(), 10, 50);


    if (polygon_selected) {
        ofNoFill();
        ofSetColor(255, 0, 0);
        ofDrawCircle(
                multigon[polygon_sh_selected][ polygon_pt_selected ].x,
                multigon[polygon_sh_selected][ polygon_pt_selected ].y,
                multigon[polygon_sh_selected][ polygon_pt_selected ].z,
                15);
    }

    if (mouse_RD) {
        ofSetColor(255, 255, 255);
        ofDrawBitmapString(
                ofToString(shrink_factor),
                10, 25);
    }
    
    ui.draw();

    return;

}

//--------------------------------------------------------------

void demoApp::keyPressed(int key) {}

//--------------------------------------------------------------

void demoApp::keyReleased(int key) {

    if (key == 32) {

        std::stringstream ss;
        ss << "roadmap_" << ofGetYear();
        if (ofGetMonth() < 10) ss << "0";
        ss << ofGetMonth();
        if (ofGetDay() < 10) ss << "0";
        ss << ofGetDay();
        if (ofGetHours() < 10) ss << "0";
        ss << ofGetHours();
        if (ofGetMinutes() < 10) ss << "0";
        ss << ofGetMinutes();
        if (ofGetSeconds() < 10) ss << "0";
        ss << ofGetSeconds();
        if (ofGetElapsedTimeMillis() < 10) ss << "0";
        else if (ofGetElapsedTimeMillis() < 100) ss << "0";
        ss << ofGetElapsedTimeMillis();
        ss << ".png";
        ofSaveViewport(ss.str());

    } else if (key == 112) { // p

        for (int i = 0; i < multigon.point_num(); ++i) {

            cout << "polygon[0][" << i <<
                    "].set(" <<
                    multigon[0][i].x << "," <<
                    multigon[0][i].y << "," <<
                    multigon[0][i].z <<
                    ");" << endl;

        }

    } else if (
            key == 48 ||
            key == 49 ||
            key == 50 ||
            key == 51 ||
            key == 52 ||
            key == 53 ||
            key == 54 ||
            key == 55 ||
            key == 56 ||
            key == 57
            ) {

        testBatch(key - 48);

    } else {

        cout << key << endl;

    }


}

//--------------------------------------------------------------

void demoApp::mouseMoved(int x, int y) {

    mouse.set(
            (x - ofGetWidth() * 0.5) / ofGetWidth(),
            (y - ofGetHeight() * 0.5) / ofGetHeight()
            );

    if (!mouse_LD) {
        polygon_selected = multigon.closest(
                polygon_sh_selected, polygon_pt_selected,
                darkcity::Vec3<float>(ofGetMouseX(), ofGetMouseY(), 0),
                20);
    }

}

//--------------------------------------------------------------

void demoApp::mouseDragged(int x, int y, int button) {

    if (polygon_selected && mouse_LD) {

        multigon[polygon_sh_selected][polygon_pt_selected] = darkcity::Vec3<float>(
                x + mouse_decal.x,
                y + mouse_decal.y,
                0);
        multigon.process();
        previous_shrink_factor = 0;

    } else if (mouse_RD) {

        shrink_factor = init_factor + (x - mouse_init.x) * 0.2;
        uisl_shrink.getParameter().cast<float>() = shrink_factor;

    }

}

//--------------------------------------------------------------

void demoApp::mousePressed(int x, int y, int button) {

    if (button == 0 && !mouse_RD) {

        mouse_LD = true;

        if (polygon_selected) {
            darkcity::Vec3<float> diff =
                    multigon[polygon_sh_selected][polygon_pt_selected] -
                    darkcity::Vec3<float>(x, y, 0);
            mouse_decal.x = diff.x;
            mouse_decal.y = diff.y;
        }

    } else if (button == 2 && !mouse_LD) {

        mouse_RD = true;
        init_factor = shrink_factor;
        mouse_init.x = x;
        mouse_init.y = y;

    }

}

//--------------------------------------------------------------

void demoApp::mouseReleased(int x, int y, int button) {

    mouse_RD = false;
    mouse_LD = false;
    polygon_selected = false;

}