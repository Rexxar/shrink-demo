/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of darkcity package, the city map generator of polymorph
 engine.
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   DCPoint.h
 * Author: frankiezafe
 *
 * Created on April 27, 2017, 8:48 PM
 */

#ifndef DARKCITY_POINT_H
#define DARKCITY_POINT_H

#include "DCVec3.h"

namespace darkcity {

    template< typename T >
    class Point : public Vec3<T> {
    public:

        // direction to next
        Vec3<T> dir;
        // normal of direction in plane perpendicular to Vec3<T>::UP
        Vec3<T> dir_normal;
        // absolute angle of direction in plane perpendicular to Vec3<T>::UP
        T dir_angle;
        // normal of the point,
        // average of previous direction and direction normals
        // up axis is forced to 0, see Vec3<T>::UP
        Vec3<T> normal;
        // absolute angle of point normal
        T normal_angle;
        // relative angle: normal angle - direction angle
        T angle;
        // shrink speed, rendered with relative angle
        T shrink_multiplier;
        // flag for concav
        bool concav;
        // keep when applying a negative shrink
        bool inflatable;
        // keep when applying a positive shrink
        bool shrinkable;
        
        // previous point in the shape
        Point* previous;
        // next point in the shape
        Point* next;
        
        // 2d version
        Vec3<T> pos_2d;
        Vec3<T> dir_2d;

        Point( ) :
        Vec3<T>( ),
        dir_angle( 0 ),
        normal_angle( 0 ),
        angle( 0 ),
        concav( false ),
        inflatable( true ),
        shrinkable( true ),
        previous( 0 ),
        next( 0 ) {}

        inline void operator=( const Point<T>& src ) {
            Vec3<T>::set( src.x, src.y, src.z );
            dir = src.dir;
            dir_normal = src.dir_normal;
            dir_angle = src.dir_angle;
            normal = src.normal;
            normal_angle = src.normal_angle;
            angle = src.angle;
            concav = src.concav;
            inflatable = src.inflatable;
            shrinkable = src.shrinkable;
            previous = 0;
            next = 0;
            pos_2d = src.pos_2d;
            dir_2d = src.dir_2d;
        }

        inline void operator=( const Vec3<T>& src ) {
            Vec3<T>::set( src.x, src.y, src.z );
        }


    };

};

#endif /* DARKCITY_POINT_H */

