#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "darkcity/DCMultigon.h"

class demoApp : public ofBaseApp {
public:
    
    void setup();
    void updateShrink();
    void update();
    void draw();
    void draw( darkcity::Multigon<float>&, float opacity = 1, bool debug = false );

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    
    void shrinkChanged( float& v );
    void toggleChanged( bool& v );

private:
    
    ofxPanel ui;
    ofxFloatSlider uisl_shrink;
    ofxToggle uitg_inward;
    ofxToggle uitg_outward;
    ofxToggle uitg_debug;
    
    uint8_t uitgs_selected;
    uint8_t uitgs_shapenum;
    bool* shapes_status;
    ofxToggle* uitgs_shapes;

    ofVec2f mouse;
    ofVec2f mouse_init;
    ofVec2f mouse_decal;
    bool mouse_RD;
    bool mouse_LD;
    
    bool shrink_in;
    bool shrink_out;
    bool shrink_debug;
    float init_factor;
    float shrink_factor;
    float previous_shrink_factor;
    
    darkcity::Multigon<float> multigon;
    darkcity::Multigon<float> multigon_cp;
    std::vector< darkcity::Multigon<float>* > mgons_in; 
    std::vector< darkcity::Multigon<float>* > mgons_out; 
    darkcity::Multigon<float> multigon_result;
    bool polygon_selected;
    uint8_t polygon_sh_selected;
    uint16_t polygon_pt_selected;
            
    void testBatch( int i );
    
};
